import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class Task extends Equatable {
  final String title;
  final String description;
  final String id;

  Task({
    @required this.title,
    @required this.description,
    this.id})   : super([title, description, id]);
}
