import 'package:dartz/dartz.dart';
import 'package:scrum_boardv3/core/error/failures.dart';
import 'package:scrum_boardv3/core/usecases/params.dart';
import 'package:scrum_boardv3/core/usecases/usecase.dart';
import 'package:scrum_boardv3/features/scrum_board/data/models/list_of_tasks_model.dart';
import 'package:scrum_boardv3/features/scrum_board/domain/repositories/task_repository.dart';

class GetList implements UseCase<ListOfTasksModel,NoParams>{
  final ScrumBoardRepository repository;
  GetList(this.repository);

  @override
  Future<Either<Failure, ListOfTasksModel>> call(NoParams params) async {
    return await repository.getList();
  }}