import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:scrum_boardv3/core/error/failures.dart';
import 'package:scrum_boardv3/core/usecases/params.dart';
import 'package:scrum_boardv3/features/scrum_board/data/models/list_of_tasks_model.dart';



abstract class ScrumBoardRepository{
  Future<Either<Failure,ListOfTasksModel>> getList();

  Future<Either<Failure,ListOfTasksModel>> addTask({
    @required String title,
    @required String description});

  Future<Either<Failure,ListOfTasksModel>> editTask({
    @required String title,
    @required String description,
    @required String id,
    @required int indexPage});

  Future<Either<Failure,ListOfTasksModel>> moveTask({
    @required String title,
    @required String description,
    @required int whereMove,
    @required int indexPage,
    @required String id});

  Future<Either<Failure,ListOfTasksModel>> deleteTask({
    @required int indexPage,
    @required String id});

}




