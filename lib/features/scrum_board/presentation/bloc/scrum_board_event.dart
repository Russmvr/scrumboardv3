import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:equatable/equatable.dart';

abstract class ScrumBoardEvent extends Equatable {
  ScrumBoardEvent([List props = const <dynamic>[]]) : super(props);
}

class StartEvent extends ScrumBoardEvent {}

class AddTaskEvent extends ScrumBoardEvent {
  final String title;
  final String description;

  AddTaskEvent({
    @required this.title,
    this.description}); // !!!!!!!!!! вот тут надо проверить. может не надо делать ее @required. может так сойдет
}


class MoveTaskEvent extends ScrumBoardEvent{
  final String title;
  final String description;
  final int whereMove;
  final int indexPage;
  final String id;

  MoveTaskEvent({
    @required this.title,
    @required this.description,
    @required this.whereMove,
    @required this.indexPage,
    @required this.id,});

}

class DeleteTaskEvent extends ScrumBoardEvent{
  final int indexPage;
  final String id;

  DeleteTaskEvent({
    @required this.indexPage,
    @required this.id});
}

class EditTaskEvent extends ScrumBoardEvent{
  final String title;
  final String description;
  final String id;
  final int indexPage;

  EditTaskEvent({
    @required this.title,
    @required this.description,
    @required this.id,
    @required this.indexPage});
}
