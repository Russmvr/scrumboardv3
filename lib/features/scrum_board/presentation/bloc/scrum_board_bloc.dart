import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:scrum_boardv3/core/error/failures.dart';
import 'package:scrum_boardv3/core/usecases/params.dart';
import 'package:scrum_boardv3/core/usecases/usecase.dart';
import 'package:scrum_boardv3/features/scrum_board/data/models/list_of_tasks_model.dart';
import 'package:scrum_boardv3/features/scrum_board/domain/usecases/add_task.dart';
import 'package:scrum_boardv3/features/scrum_board/domain/usecases/edit_task.dart';
import 'package:scrum_boardv3/features/scrum_board/domain/usecases/move_task.dart';
import 'package:scrum_boardv3/features/scrum_board/domain/usecases/delete_task.dart';
import 'package:scrum_boardv3/features/scrum_board/domain/usecases/get_list.dart';
import './bloc.dart';

const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The number must be a positive integer or zero.';

class ScrumBoardBloc extends Bloc<ScrumBoardEvent, ScrumBoardState> {
  final AddTask addTask;
  final DeleteTask deleteTask;
  final EditTask editTask;
  final MoveTask moveTask;
  final GetList getList;


  ScrumBoardBloc({
    this.addTask,
    this.deleteTask,
    this.editTask,
    this.moveTask,
    this.getList,
  }) ;



  @override
  ScrumBoardState get initialState => Empty();

  @override
  Stream<ScrumBoardState> mapEventToState(ScrumBoardEvent event) async* {
    if (event is StartEvent) {
      yield Loading();
      final failureOrList = await getList(NoParams()); //пока делаю по старому. без использования стейта Start. Потом попробовать поменять.
      print('Что пришло в failureORList --> $failureOrList');
      yield* _eitherLoadedOrErrorState(failureOrList);
    }

    if (event is AddTaskEvent) {
      final failureOrList = await addTask(
          ParamsToRepo(title: event.title, description: event.description)
      ); //!!! тут туплю
      yield* _eitherLoadedOrErrorState(failureOrList);
    }
    if (event is DeleteTaskEvent) {
      final failureOrList = await deleteTask(
          ParamsToRepo(id: event.id, indexPage: event.indexPage)
      ); //!!! тут туплю
      yield* _eitherLoadedOrErrorState(failureOrList);
    }

    if (event is EditTaskEvent) {
      final failureOrList = await editTask(
          ParamsToRepo(title: event.title,
            description: event.description,
            indexPage: event.indexPage,
            id: event.id)
      ); //!!! тут туплю
      yield* _eitherLoadedOrErrorState(failureOrList);
    }
    if (event is MoveTaskEvent) {
      final failureOrList = await moveTask(
          ParamsToRepo(
              title: event.title,
              description: event.description,
              indexPage: event.indexPage,
              id: event.id,
              whereMove: event.whereMove)); //!!! тут туплю
      yield* _eitherLoadedOrErrorState(failureOrList);
    }

  }


  Stream<ScrumBoardState> _eitherLoadedOrErrorState(
      Either<Failure, ListOfTasksModel> either,
      ) async* {
    yield either.fold(
          (failure) => Error(message: _mapFailureToMessage(failure)),   // тоже самое
          (listSuccess) => Loaded(list: listSuccess),
    );
  }


  String _mapFailureToMessage(Failure failure) {
    // Instead of a regular 'if (failure is ServerFailure)...'
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      default:
        return 'Unexpected Error';
    }
  }



}
