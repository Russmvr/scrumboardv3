import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:scrum_boardv3/features/scrum_board/data/models/list_of_tasks_model.dart';


@immutable
abstract class ScrumBoardState extends Equatable {
  ScrumBoardState([List props = const <dynamic>[]]) : super(props);
}

class Empty extends ScrumBoardState {}

class Loading extends ScrumBoardState {}

class LoadingInsideWindow extends ScrumBoardState {}

class Loaded extends ScrumBoardState {
  final ListOfTasksModel list;

  Loaded({@required this.list}) : super([list]);
}

class Error extends ScrumBoardState {
  final String message;

  Error({@required this.message}) : super([message]);
}