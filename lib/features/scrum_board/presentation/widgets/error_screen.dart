
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scrum_boardv3/features/scrum_board/presentation/bloc/scrum_board_bloc.dart';
import 'package:scrum_boardv3/features/scrum_board/presentation/bloc/scrum_board_event.dart';

// ignore: non_constant_identifier_names, missing_return
Widget errorScreen(context, message)  {
  String titleValue;
  String descriptionValue;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final titleFocusNode = FocusNode();
  final bool _isLoading = false;

  showDialog(
      context: context,
      builder: (_) => AlertDialog(
        content: Builder(
          builder: (context) {
            var height = MediaQuery.of(context).size.height;
            var width = MediaQuery.of(context).size.width;
            return Container(
              height: height - 500,
              width: width - 100,
              child: Center(child: Text('$message',  style: TextStyle(fontFamily: 'Raleway', fontSize: 25, fontWeight: FontWeight.w500)) ),
            );
          },
        ),
      ));
}









