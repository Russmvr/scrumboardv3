import 'package:flutter/material.dart';

import 'detail_task.dart';

// ignore: must_be_immutable
class GridViewTasks extends StatefulWidget {
  final List<dynamic> allList;
  final int indexPageView;
  final state;
  final contextK;

  GridViewTasks({this.allList,this.indexPageView,this.state, this.contextK, Key key}) : super(key: key);

  @override
  _GridViewTasksState createState() => _GridViewTasksState();
}

class _GridViewTasksState extends State<GridViewTasks> {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
//        physics: NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          childAspectRatio: 1.05,
        ),
        itemCount: ()
        {
          for(int i=0;i<4;i++){
            if(widget.indexPageView == i){
              return widget.allList[i].length;
            }
          }
        }
        (),
        itemBuilder: (context, indexGridView) {
          return Container(
            width: 30,
            height: 30,
            child: Card(
              child: InkWell(
                child: () {
                  for(int i=0;i<4;i++){
                    if(widget.indexPageView == i){
                      return Center(
                          child: Text(widget.allList[i][indexGridView].title,
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                              ),
                              textAlign: TextAlign.center));
                    }
                  }
                }(),
                onTap: ()
                {
                  detailTask(context, widget.state, widget.indexPageView,
                      indexGridView, widget.allList, widget.contextK);
                },
              ),
              elevation: 8,
              color: () {
                switch (widget.indexPageView) {
                  case 0:
                    return Color.fromARGB(255, 79, 173, 232);
                  case 1:
                    return Color.fromARGB(255, 232, 79, 79);
                  case 2:
                    return Color.fromARGB(255, 232, 148, 79);
                  case 3:
                    return Color.fromARGB(255, 79, 232, 130);
                }
              }(),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
            ),
          );
        });
  }
}
