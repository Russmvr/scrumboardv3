import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scrum_boardv3/features/scrum_board/presentation/bloc/bloc.dart';
import 'package:scrum_boardv3/features/scrum_board/presentation/bloc/scrum_board_bloc.dart';
import 'package:scrum_boardv3/features/scrum_board/presentation/widgets/edit_task_widget.dart';


void detailTask(context, state,indexPageView,indexGridView,allListTasks, contextK,){
  showDialog(
      context: context,
      builder: (_) => AlertDialog(
        insetPadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15.0))),
        content: Builder(builder: (context){
          var height = MediaQuery.of(context).size.height;
          var width = MediaQuery.of(context).size.width;
          return Container(
            height: height - 340,
            width: width,
            child: ListView(
              children: [(){
                for(int i=0;i<4;i++){
                  if(indexPageView == i){
                    return Row(
                      children: [
                        Container(
                            child: IconButton(
                              icon: Icon(Icons.edit,
                                  color: Colors.white,              //Это такая поебота. Я изначально хотел сделать Row и у него 2 child'а. Текст и кнопка
                                  size: 28),//                      Но Почему-то текст не ровно вставал в центр. Я там и Center пробовал, и Alignment, и padding и margin.
                              onPressed: null,            //  Не помогало. Топики других колонок не стояли точно по центру, съезжали. Поэтому пошел гуглить и нашел этот лайфхак со Spacer
                              // Немного нехорошо, что я это не смог сделать более нормальным способом. Я уж не батя верстки. Но главное работает.
                            )),
                        Spacer(),
                        Center(child: Text(allListTasks[i][indexGridView].title,
                          style: TextStyle(fontFamily: 'Raleway', fontSize: 28, decoration: TextDecoration.underline),)),
                        Spacer(),
                        Container(
                            child: IconButton(
                              icon: Icon(Icons.edit,
                                  color: Colors.grey.shade700,              //Это такая поебота. Я изначально хотел сделать Row и у него 2 child'а. Текст и кнопка
                                  size: 28),//                      Но Почему-то текст не ровно вставал в центр. Я там и Center пробовал, и Alignment, и padding и margin.
                              onPressed:() {
                                Navigator.pop(context);
                                editTaskWidget(
                                    context,
                                    contextK,
                                    allListTasks[indexPageView][indexGridView].id,
                                    indexPageView,
                                    allListTasks[i][indexGridView].title,
                                    allListTasks[i][indexGridView].description,
                                );
                                }// Немного нехорошо, что я это не смог сделать более нормальным способом. Я уж не батя верстки. Но главное работает.
                                )),
                      ],
                    );
                  }
                }
              }(),
                SizedBox(height: 40,),
                    (){
                      for(int i=0;i<4;i++){
                        if(indexPageView == i){
                          return Text(allListTasks[i][indexGridView].description,
                            style: TextStyle(fontFamily: 'Raleway', fontSize: 24,),textAlign: TextAlign.center,);
                        }
                      }
                }(),
                SizedBox(height: 50,),
                Center(child: Text('Move to:',style: TextStyle(fontFamily: 'Raleway',fontSize: 20),)),
                Row(mainAxisAlignment: MainAxisAlignment.center ,children: [
                  indexPageView == 0 ? Visibility(visible: false, child: Text('gone'),):RaisedButton(child:Text('Back Log',style: TextStyle(fontSize: 17),),onPressed: (){
                    BlocProvider.of<ScrumBoardBloc>(contextK)
                        .dispatch(MoveTaskEvent(whereMove: 0, indexPage: indexPageView,
                      id: allListTasks[indexPageView][indexGridView].id,
                    title: allListTasks[indexPageView][indexGridView].title,
                    description: allListTasks[indexPageView][indexGridView].description));
                    Future.delayed(Duration(milliseconds: 2100),() =>Navigator.pop(context));
                  },),
                  indexPageView == 1 ? Visibility(visible: false, child: Text('gone'),):RaisedButton(child:Text('To Do',style: TextStyle(fontSize: 17)),onPressed: (){
                    BlocProvider.of<ScrumBoardBloc>(contextK)
                        .dispatch(MoveTaskEvent(whereMove: 1, indexPage: indexPageView,
                        id: allListTasks[indexPageView][indexGridView].id,
                        title: allListTasks[indexPageView][indexGridView].title,
                        description: allListTasks[indexPageView][indexGridView].description));
                    Future.delayed(Duration(milliseconds: 2100),() =>Navigator.pop(context));                                             // -----> Это всё кнопки перемещения задачи из одной колонки в другую. Также ивенты разные
                  },),
                  indexPageView == 2 ? Visibility(visible: false, child: Text('gone'),):RaisedButton(child:Text('In Progress',style: TextStyle(fontSize: 17)),onPressed: (){
                    BlocProvider.of<ScrumBoardBloc>(contextK)
                        .dispatch(MoveTaskEvent(whereMove: 2, indexPage: indexPageView,
                        id: allListTasks[indexPageView][indexGridView].id,
                        title: allListTasks[indexPageView][indexGridView].title,
                        description: allListTasks[indexPageView][indexGridView].description));
                    Future.delayed(Duration(milliseconds: 2100),() => Navigator.pop(context));
                  },),
                  indexPageView == 3 ? Visibility(visible: false, child: Text('gone'),):RaisedButton(child:Text('Done',style: TextStyle(fontSize: 17)),onPressed: (){
                    BlocProvider.of<ScrumBoardBloc>(contextK)
                        .dispatch(MoveTaskEvent(whereMove: 3, indexPage: indexPageView,
                        id: allListTasks[indexPageView][indexGridView].id,
                        title: allListTasks[indexPageView][indexGridView].title,
                        description: allListTasks[indexPageView][indexGridView].description));
                    Future.delayed(Duration(milliseconds: 2100),() =>Navigator.pop(context));
                  },),
                ],),
                SizedBox(height: 20,),
                Center(child: Text('Delete this task:',style: TextStyle(fontFamily: 'Raleway',fontSize: 20),)),
                SizedBox(height: 5,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RaisedButton(color:Colors.red,child:Text('DELETE'),onPressed: (){
                      BlocProvider.of<ScrumBoardBloc>(contextK)
                          .dispatch(DeleteTaskEvent(id: allListTasks[indexPageView][indexGridView].id, indexPage: indexPageView));
                      Future.delayed(Duration(milliseconds: 800),() =>Navigator.pop(context));
                    },),
                  ],
                ),
              ],
            ),);
        },),));
}