
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scrum_boardv3/features/scrum_board/presentation/bloc/scrum_board_bloc.dart';
import 'package:scrum_boardv3/features/scrum_board/presentation/bloc/scrum_board_event.dart';

// ignore: non_constant_identifier_names
Future<void> editTaskWidget(context,contextK, id, indexPage, title, description) async {
  String titleValue;
  String descriptionValue;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final titleFocusNode = FocusNode();
//  final bool _isLoading = false;
  final controllerTitle = TextEditingController(text: '$title');
  final controllerDescription = TextEditingController(text: '$description');


  showDialog(
      context: context,
      builder: (_) => AlertDialog(
        insetPadding: EdgeInsets.all(15),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15.0))),
        content: Builder(
          builder: (context) {
            var height = MediaQuery.of(context).size.height;
            var width = MediaQuery.of(context).size.width;
            return Container(
              height: height - 325,
              width: width - 50,
              child: Form(
                key: _formKey,
                child: ListView(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      child: Text(
                        'Edit Task',
                        style: TextStyle(fontFamily: 'Raleway', fontSize: 30, fontWeight: FontWeight.w500),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextFormField(
                        controller: controllerTitle,
                        focusNode: titleFocusNode,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                          alignLabelWithHint: true,
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black,width: 1.1)),
                          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black,width: 1.1)),
                          hintText:'Title',
                          hintStyle: TextStyle(
                            color: Colors.black54, fontFamily: 'Raleway', fontSize: 22,
                          ),
                        ),
                        validator: (String value){
                          if(value.isEmpty != false){
                            return "The title should not be empty";
                          }
                        },
                        onSaved: (String value){
                          titleValue = value;
                        },
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextFormField(
                        controller: controllerDescription,
                        maxLines:5,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                          alignLabelWithHint: true,
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black,width: 1.1)),
                          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black,width: 1.1)),
                          hintText: 'Description',
                          hintStyle: TextStyle(
                            color: Colors.black54, fontFamily: 'Raleway', fontSize: 22,
                          ),
                        ),
                        onSaved: (String value){
                          descriptionValue = value;
                        },
                      ),
                    ),
                    SizedBox(height: 30,),
                    Row(mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(width: 120,height: 40,child: RaisedButton(child:Text('Save'),onPressed: () {
                          if (!_formKey.currentState.validate()){
                            return;
                          }
                          _formKey.currentState.save();
                          BlocProvider.of<ScrumBoardBloc>(contextK)
                              .dispatch(EditTaskEvent(title: titleValue, description: descriptionValue,id: id ,indexPage: indexPage));
                          Future.delayed(Duration(milliseconds: 1900),() =>Navigator.pop(context));
                        })),
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ));
}









