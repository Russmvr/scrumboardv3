
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scrum_boardv3/features/scrum_board/presentation/bloc/scrum_board_bloc.dart';
import 'package:scrum_boardv3/features/scrum_board/presentation/bloc/scrum_board_event.dart';

// ignore: non_constant_identifier_names
Future<void> LoadingWidget(context) async {
  String titleValue;
  String descriptionValue;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final titleFocusNode = FocusNode();

  showDialog(
      context: context,
      builder: (_) => AlertDialog(
        insetPadding: EdgeInsets.all(15),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15.0))),
        content: Builder(
          builder: (context) {
            var height = MediaQuery.of(context).size.height;
            var width = MediaQuery.of(context).size.width;    // кружочек
            return Container(
                height: height - 325,
                width: width - 50,
                child: Center(child: CircularProgressIndicator())
            );
          },
        ),
      ));
}
