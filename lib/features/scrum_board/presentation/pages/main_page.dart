import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:scrum_boardv3/features/scrum_board/presentation/bloc/scrum_board_bloc.dart';
import 'package:scrum_boardv3/features/scrum_board/presentation/bloc/scrum_board_event.dart';
import 'package:scrum_boardv3/features/scrum_board/presentation/bloc/scrum_board_state.dart';
import 'package:scrum_boardv3/features/scrum_board/presentation/widgets/add_task_widget.dart';
import 'package:scrum_boardv3/features/scrum_board/presentation/widgets/error_screen.dart';
import 'package:scrum_boardv3/features/scrum_board/presentation/widgets/gridview_tasks.dart';
import 'package:scrum_boardv3/features/scrum_board/presentation/widgets/loading_widget.dart';

import '../../../../injection_container.dart';

ScrumBoardBloc bloc = ScrumBoardBloc();
class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      body: BlocProvider(builder: (context) => sl<ScrumBoardBloc>(), //проверь  тест с контекст
        child: BlocBuilder<ScrumBoardBloc,ScrumBoardState>(
          // ignore: missing_return
          builder: (contextK,state){ // Все твои ебаные BlocProvider'ы должны совпадать с этим  <------ Контекстом
            if(state is Empty){
              return Center(child: RaisedButton(child: Text('HI'), onPressed: (){
                BlocProvider.of<ScrumBoardBloc>(contextK)
                    .dispatch(StartEvent());
              },));
            } else if(state is Loading){
              return Center(child: CircularProgressIndicator());
            } else if(state is Loaded){
              final allList = state.list.listOfTasks;
              return ScrumBoardDisplay(list: allList, state: state, contextK: contextK,);
            } else if(state is Error){
              return AlertDialog(title: Text('ERROR'), content: Text('${state.message}'), actions: [FlatButton(onPressed: (){Navigator.pop(context);}, child: Text('OK'))], );
            }
          },
        ),
      ) ,
    );


  }
}

class ScrumBoardDisplay extends StatefulWidget {
  final List<dynamic> list;
  final state;
  final dynamic contextK;

  const ScrumBoardDisplay({Key key, this.list, this.state, this.contextK}) : super(key: key);

  @override
  _ScrumBoardDisplayState createState() => _ScrumBoardDisplayState();
}

class _ScrumBoardDisplayState extends State<ScrumBoardDisplay> {
  final List<String> _topics = ['Back Log', 'To Do', 'In Progress', 'Done'];
  PageController _pageController = PageController(initialPage: 0);


  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return PageView.builder(
      controller: _pageController,
      itemCount: 4,
      itemBuilder: (context, indexPageView) {
        print('СТРАНИЦА - $indexPageView');
        return SingleChildScrollView(
          child: Column(children: [
            SizedBox(height: 30),
            Row(
              children: [
                Container(
                    padding: EdgeInsets.only(),
                    child: IconButton(
                      icon: Icon(Icons.add,
                          color: Colors.grey.shade200,              //Это такая поебота. Я изначально хотел сделать Row и у него 2 child'а. Текст и кнопка
                          size: 32),//                      Но Почему-то текст не ровно вставал в центр. Я там и Center пробовал, и Alignment, и padding и margin.
                      onPressed: null,            //  Не помогало. Топики других колонок не стояли точно по центру, съезжали. Поэтому пошел гуглить и нашел этот лайфхак со Spacer
                      // Немного нехорошо, что я это не смог сделать более нормальным способом. Я уж не батя верстки. Но главное работает.
                    )),
                Spacer(),
                Container(
                    child: Text(
                      _topics[indexPageView],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: 'Raleway', fontSize: 36,),
                    )),
                Spacer(),
                Container(
                    padding: EdgeInsets.only(),
                    child: IconButton(
                      icon: Icon(Icons.add,
                          color: indexPageView == 0
                              ? Colors.black
                              : Colors.grey.shade200,
                          size: indexPageView == 0 ? 32 : 32),
                      onPressed: indexPageView == 0
                          ? () {
                                    addTaskWidget(context, widget.contextK); // ---> Смотри add_task. Это добавление задачи. КНОПКА добавления задачи <---
                      }
                          : null,
                    )),
              ],
            ),
            Divider(
              color: Colors.black,
              thickness: 2,
              indent: 100,
              endIndent: 100,
            ),
            Container(
                width: width,
                height: height-100,
                padding: EdgeInsets.all(14),
                child: GridViewTasks(
                  allList: widget.list,
                  state: widget.state,
                  indexPageView: indexPageView,contextK: widget.contextK,) //  ---> Смотри gridview_tasks. Тут добавляем Gridview.builder. Т.е показываем наши задачи <---
            ),
          ]),
        );
      },
    );
  }
}


