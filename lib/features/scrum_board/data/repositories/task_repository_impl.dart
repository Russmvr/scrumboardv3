import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:scrum_boardv3/core/error/exceptions.dart';
import 'package:scrum_boardv3/core/error/failures.dart';
import 'package:scrum_boardv3/core/network/network_info.dart';
import 'package:scrum_boardv3/features/scrum_board/data/datasources/task_local_data_source.dart';
import 'package:scrum_boardv3/features/scrum_board/data/datasources/task_remote_data_sources.dart';
import 'package:scrum_boardv3/features/scrum_board/data/models/list_of_tasks_model.dart';
import 'package:scrum_boardv3/features/scrum_board/domain/repositories/task_repository.dart';

typedef Future<ListOfTasksModel> _Chooser();

class ScrumBoardRepositoryImpl implements ScrumBoardRepository {
  final ListOfTasksRemoteDataSource remoteDataSource;
  final ListOfTasksLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  ScrumBoardRepositoryImpl({
    @required this.remoteDataSource,
    @required this.localDataSource,
    @required this.networkInfo});

  Future<Either<Failure, ListOfTasksModel>> _getListMethod(_Chooser chooser) async{
    {
      if (await networkInfo.isConnected) {
        try {
          final remoteList = await chooser();     // в эту штуку будет приходить numbertriviamodel объект
          localDataSource.cacheList(remoteList);    // это строчка закидывает на диск/кэш последний использованный numbertrivia число
          return Right(remoteList);
        } on ServerException {            // Строка on ServerException? как она читается работает? Обычно я видел конструкцию try - catch  |---|  // гениально. Он создал отдельный класс "исключений" в который будут попадать объекты с ошибками.
          return Left(ServerFailure());   // Ну вдруг не сработает какой либо код, напр:
        }                                   // final remoteTrivia = await getConcreteOrRandom();  и тд, и тоже самое ниже
      } else {
        try {
          final localList = await localDataSource.getLastList();        //Он берет последнее число с кэша. Дальше отправляет его навверх
          return Right(localList);    // отправляет навверх
        } on CacheException {
          return Left(CacheFailure());
        }
      }
    }
  }


  @override
  Future<Either<Failure, ListOfTasksModel>> getList() async {
    return await _getListMethod(() {
      return remoteDataSource.getList();
    });
  }

  @override
  Future<Either<Failure, ListOfTasksModel>> addTask({String title,
    String description}) async  {
    return await _getListMethod((){
      return remoteDataSource.addTask(title: title, description: description);
    }
    );
  }

  @override
  Future<Either<Failure, ListOfTasksModel>> deleteTask ({int indexPage, String id}) async {
    return await _getListMethod((){                                                     // ЧТОБЫ СРАБОТАЛ ТАКОЙ ВИД { ....
      return remoteDataSource.deleteTask(id: id, indexPage: indexPage);                                   // return ..} нужно чтобы была заглушка. В данном случае, заглушка - ()
    }
    );
  }



  @override
  Future<Either<Failure, ListOfTasksModel>> editTask({@required String title, @required String description, @required String id, @required int indexPage}) async {
    return await _getListMethod((){
      return remoteDataSource.editTask(title: title, description: description, id: id, indexPage: indexPage);
    }
    );
  }


  @override
  Future<Either<Failure, ListOfTasksModel>> moveTask({String title, String description, int whereMove, int indexPage, String id}) async {
    return await _getListMethod((){
      return remoteDataSource.moveTask(title: title, description: description, whereMove: whereMove, indexPage: indexPage, id: id);
    }
    );
  }


//  @override
//  Future<Either<Failure, ListOfTasksModel>> getList() async {
//    return await _getListMethod(() {
//      return remoteDataSource.getList();
//    });
//  }
//
//  @override
//  Future<Either<Failure, ListOfTasksModel>> addTask({String title,
//    String description}) async  {
//    return await _getListMethod((){
//      return remoteDataSource.addTask(title: title, description: description);
//    }
//    );
//  }
//
//  @override
//  Future<Either<Failure, ListOfTasksModel>> deleteTask ({int indexPage, String id}) async {
//    return await _getListMethod((){                                                     // ЧТОБЫ СРАБОТАЛ ТАКОЙ ВИД { ....
//      return remoteDataSource.deleteTask(id: id, indexPage: indexPage);                                   // return ..} нужно чтобы была заглушка. В данном случае, заглушка - ()
//    }
//    );
//  }
//
//
//
//  @override
//  Future<Either<Failure, ListOfTasksModel>> editTask({@required String title, @required String description, @required String id, @required int indexPage}) async {
//    return await _getListMethod((){
//      return remoteDataSource.editTask(title: title, description: description, id: id, indexPage: indexPage);
//    }
//    );
//  }
//
//
//  @override
//  Future<Either<Failure, ListOfTasksModel>> moveTask({String title, String description, int whereMove, int indexPage, String id}) async {
//    return await _getListMethod((){
//      return remoteDataSource.moveTask(title: title, description: description, whereMove: whereMove, indexPage: indexPage, id: id);
//  }
//    );
//  }

}

