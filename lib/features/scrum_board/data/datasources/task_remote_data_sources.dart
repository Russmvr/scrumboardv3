import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:scrum_boardv3/core/error/exceptions.dart';
import 'package:scrum_boardv3/features/scrum_board/data/models/list_of_tasks_model.dart';
import 'package:http/http.dart' as http;

abstract class ListOfTasksRemoteDataSource{
  Future <ListOfTasksModel> getList();
  Future <ListOfTasksModel> addTask({String title,
    String description});
  Future <ListOfTasksModel> moveTask({String title,
    String description, int whereMove, int indexPage, String id});
  Future <ListOfTasksModel> deleteTask({int indexPage,String id});
  Future <ListOfTasksModel> editTask({@required String title, @required String description, @required String id, @required int indexPage});
}

const String urlLIST = 'https://scrumboard-68655.firebaseio.com/allTasks.json';

class ListOfTasksRemoteDataSourceImpl implements ListOfTasksRemoteDataSource {
  @override
  Future<ListOfTasksModel> getList() =>
      _getListFromUrl(urlList: urlLIST);

  @override
  Future<ListOfTasksModel> addTask({String title,
      String description}) => _addTask(
      urlTo:'https://scrumboard-68655.firebaseio.com/allTasks/0.json',
      urlList: urlLIST,
      title: title,
      description: description
    );

  @override
  Future<ListOfTasksModel> moveTask({String title,
    String description, int whereMove, int indexPage, String id}) => _moveTask(
    urlFrom: 'https://scrumboard-68655.firebaseio.com/allTasks/$indexPage/$id.json',
    urlTo: 'https://scrumboard-68655.firebaseio.com/allTasks/$whereMove.json',
    urlList: urlLIST,
    title: title,
    description: description,);

  @override
  Future<ListOfTasksModel> deleteTask({int indexPage,String id}) => _deleteTask(
      urlForDelete:'https://scrumboard-68655.firebaseio.com/allTasks/$indexPage/$id.json',
      urlList: urlLIST);

  @override
  Future<ListOfTasksModel> editTask({String title, String description, String id, int indexPage}) => _editTask(
    urlForEdit: 'https://scrumboard-68655.firebaseio.com/allTasks/$indexPage/$id.json',
    urlList: urlLIST,
    title: title,
    description: description,
    id: id
  );





  Future<ListOfTasksModel> _getListFromUrl({@required String urlList}) async {
    final response = await http.get(urlList);
    if (response.statusCode == 200) {
      return ListOfTasksModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }

  Future<ListOfTasksModel> _addTask({@required String urlTo, @required String urlList, @required String title, @required String description}) async {
    final responseForAddTask = await http.post(urlTo,body: json.encode({
      'title':title,
      'description':description,}));
    if(responseForAddTask.statusCode == 200){
      final extractedId = json.decode(responseForAddTask.body);
      await http.patch('https://scrumboard-68655.firebaseio.com/allTasks/0/${extractedId['name']}.json', body: json.encode({
        'title':title,
        'description':description,
        'id': extractedId['name'],
      }));
      return _getListFromUrl(urlList: urlList);
    } else {
      throw ServerException();
    }

  }

  Future<ListOfTasksModel> _moveTask({
    @required String title,
    @required String description,
    @required String urlList,
    @required String urlFrom,
    @required String urlTo}) async{

    final responseForAddTask = await http.post(urlTo,body: json.encode({
      'title':title,
      'description':description,}));
    if(responseForAddTask.statusCode == 200){
      final extractedId = json.decode(responseForAddTask.body);
      await http.patch('https://scrumboard-68655.firebaseio.com/allTasks/0/${extractedId['name']}.json', body: json.encode({
        'title':title,
        'description':description,
        'id': extractedId['name'],
      }));
      await http.delete(urlFrom);
      return _getListFromUrl(urlList: urlList); // либо я вписываю это сюда. Либо после проверки на статус код.
    } else {                          // Когда будет готов presentation надо будет посмотреть различия и тонкости
      throw ServerException();      //Возможно будет косяк, когда делается запрос, он проваливается, и весь список пропадает.
    }                             // скорее всего так и будет
  }


  Future<ListOfTasksModel> _deleteTask({@required String urlForDelete, @required String urlList}) async {
    final responseForDelete = await http.delete(urlForDelete);
    if(responseForDelete.statusCode == 200){
      return _getListFromUrl(urlList: urlList);
    } else {
      throw ServerException();
    }
  }

  Future<ListOfTasksModel> _editTask({@required String urlForEdit, @required String urlList, @required String title, @required String description, @required String id}) async {
  final responseForPatch = await http.patch(urlForEdit, body: json.encode({
    'title':title,
    'description':description,
    'id': id,
  }));
  if(responseForPatch.statusCode == 200){
    return _getListFromUrl(urlList: urlList);
  } else{
    throw ServerException();
  }
  }
}





//abstract class ScrumBoardRepository{
//  Future<Either<Failure,ListOfTasks>> addTask({String title, String description});
//  Future<Either<Failure,ListOfTasks>> editTask();
//  Future<Either<Failure,ListOfTasks>> moveTask({String title,
//      String description, int whereMove, int indexPage, String id} );
//  Future<Either<Failure,ListOfTasks>> deleteTask({int indexPage, int indexTask, String id});
//
//}