import 'dart:convert';

import 'package:scrum_boardv3/core/error/exceptions.dart';
import 'package:scrum_boardv3/features/scrum_board/data/models/list_of_tasks_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class ListOfTasksLocalDataSource{
  Future<ListOfTasksModel> getLastList();
  Future<void> cacheList(ListOfTasksModel listToCache);
}

const CACHED_BACK_LOG = 'CACHED_BACK_LOG';
const CACHED_TO_DO = 'CACHED_TO_DO';
const CACHED_IN_PROGRESS = 'CACHED_IN_PROGRESS';
const CACHED_DONE = 'CACHED_DONE';


class ListOfTasksLocalDataSourceImpl implements ListOfTasksLocalDataSource{
  final SharedPreferences sharedPreferences;

  ListOfTasksLocalDataSourceImpl({this.sharedPreferences});

  @override
  Future<ListOfTasksModel> getLastList() {
    final List<dynamic> jsonListString = [
      json.decode(sharedPreferences.getString(CACHED_BACK_LOG)),
      json.decode(sharedPreferences.getString(CACHED_TO_DO)),
      json.decode(sharedPreferences.getString(CACHED_IN_PROGRESS)),
      json.decode(sharedPreferences.getString(CACHED_DONE)),
    ];
    if(jsonListString != null){
      return Future.value(ListOfTasksModel.fromLocal(jsonListString));
    } else {
      throw CacheException();
    }
  }

  @override
  // ignore: missing_return
  Future<void> cacheList(ListOfTasksModel listToCache) {
    sharedPreferences.setString(CACHED_BACK_LOG,
        json.encode(listToCache.toJson(listToCache.listOfTasks[0])));
    sharedPreferences.setString(CACHED_TO_DO,
        json.encode(listToCache.toJson(listToCache.listOfTasks[1])));
    sharedPreferences.setString(CACHED_IN_PROGRESS,
        json.encode(listToCache.toJson(listToCache.listOfTasks[2])));
    sharedPreferences.setString(CACHED_DONE,
        json.encode(listToCache.toJson(listToCache.listOfTasks[3])));
  }
}

