
import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:scrum_boardv3/features/scrum_board/domain/entities/task.dart';

class ListOfTasksModel extends Equatable {

  final List<List<Task>> listOfTasks;

  ListOfTasksModel({this.listOfTasks}) : super(listOfTasks);

  factory ListOfTasksModel.fromJson(List<dynamic> json){
    final extractedData = json;
    List<List<Task>> allList = [[], [], [], []];
    for (int i = 0; i < 4; i++) {
      extractedData[i].remove('const');
      extractedData[i].forEach((String taskId, taskData) {
        allList[i].add(Task(
            id: taskId,
            title: taskData['title'],
            description: taskData['description']));
      });
    }
    return ListOfTasksModel(listOfTasks: allList); // [ [task],[task],[task],[task], ]
  }


//  List<List<Map>> toJson(list) {  //преобразовывает с Task в Map
//    List<List<Map>> allList2 = [[],[],[],[]];
//    for (int i=0; i<4;i++){
//      list[i].map((value) {
//        allList2[i].add({
//          'id': value.id,
//          'title': value.title,
//          'description': value.description});
//      }).toList();
//    }
//    return allList2;
//  }

  List<dynamic> toJson(list) {  //преобразовывает с Task в Map
    List<dynamic> allList = [];
    list.map((value) {
      allList.add({
        'id': value.id,
        'title': value.title,
        'description': value.description});
    }).toList();
    return allList;
  }

//  factory ListOfTasksModel.fromLocal(List<dynamic> list){ // преобразовывает с MAP{} в Task
//    final dynamic newList = [[],[],[],[]];
//    for(int k=0; k<4; k++){
//      json.decode(newList[k]);
//    }
//    List<List<Task>> allList = [[], [], [], []];
//    for (int i = 0; i < 4; i++) {
//      newList[i].map((value) {
//        allList[i].add(Task(
//            id: value['id'],
//            title: value['title'],
//            description: value['description'])); // для 1-го листа
//      }).toList();
//    }
//    return ListOfTasksModel(listOfTasks: allList);
//  }

  factory ListOfTasksModel.fromLocal(List<dynamic> list){ // преобразовывает с MAP{} в Task
    List<List<Task>> allList = [[], [], [], []];
    for (int i = 0; i < 4; i++) {
      list[i].map((value) {
        allList[i].add(Task(
            id: value['id'],
            title: value['title'],
            description: value['description'])); // для 1-го листа
      }).toList();
    }
    return ListOfTasksModel(listOfTasks: allList);
  }
}