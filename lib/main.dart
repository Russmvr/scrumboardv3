import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'features/scrum_board/presentation/pages/main_page.dart';
import 'injection_container.dart' as di;


void main() async{
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,               // Status Bar
  ));
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MainPage(),
    );
  }
}