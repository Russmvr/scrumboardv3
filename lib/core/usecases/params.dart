import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';


class ParamsToRepo extends Equatable{
  final String title;
  final String description;
  final String id;
  final int indexPage;
  final int whereMove;

  ParamsToRepo({
      this.id,
      this.indexPage,
      this.whereMove,
      this.title,
      this.description});
}


