
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:scrum_boardv3/features/scrum_board/data/datasources/task_local_data_source.dart';
import 'package:scrum_boardv3/features/scrum_board/data/datasources/task_remote_data_sources.dart';
import 'package:scrum_boardv3/features/scrum_board/data/repositories/task_repository_impl.dart';
import 'package:scrum_boardv3/features/scrum_board/domain/repositories/task_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'core/network/network_info.dart';
import 'features/scrum_board/domain/usecases/add_task.dart';
import 'features/scrum_board/domain/usecases/delete_task.dart';
import 'features/scrum_board/domain/usecases/edit_task.dart';
import 'features/scrum_board/domain/usecases/get_list.dart';
import 'features/scrum_board/domain/usecases/move_task.dart';
import 'features/scrum_board/presentation/bloc/scrum_board_bloc.dart';

final sl = GetIt.instance;

Future<void> init() async{
  //! Features - ScrumBoardBloc
  sl.registerFactory(() => ScrumBoardBloc(
    addTask: sl(),
    deleteTask: sl(),
    editTask: sl(),
    getList: sl(),
    moveTask: sl(),
    ));


  //! Core
  sl.registerLazySingleton(() => AddTask(sl()));
  sl.registerLazySingleton(() => DeleteTask(sl()));
  sl.registerLazySingleton(() => EditTask(sl()));
  sl.registerLazySingleton(() => GetList(sl()));
  sl.registerLazySingleton(() => MoveTask(sl()));
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  //! Repository
  sl.registerLazySingleton<ScrumBoardRepository>(
        () => ScrumBoardRepositoryImpl(
      remoteDataSource: sl(),
      localDataSource: sl(),
      networkInfo: sl(),
    ),
  );


  //! Data sources
  sl.registerLazySingleton<ListOfTasksRemoteDataSource>(
          () => ListOfTasksRemoteDataSourceImpl());

  sl.registerLazySingleton<ListOfTasksLocalDataSource>(
      () =>ListOfTasksLocalDataSourceImpl(sharedPreferences: sl()));

  //! External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => DataConnectionChecker());

}



